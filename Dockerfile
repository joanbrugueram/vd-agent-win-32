# Quickstart: docker build . -t vdagent_win32_builder
#             docker run --rm vdagent_win32_builder sh -c "cd /usr/x86_64-w64-mingw32/bin/ && zip - vdagent.exe vdservice.exe" > vdagent.zip
FROM debian:bullseye-slim

RUN apt-get update \
 && apt-get install -y --no-install-recommends g++-mingw-w64 libz-mingw-w64-dev \
    make autoconf autoconf-archive automake pkg-config \
    wget ca-certificates git zip \
 && rm -rf /var/lib/apt/lists/*
WORKDIR /code

# Download libpng
ARG LIBPNG_VERSION=1.6.38
RUN wget --progress=dot:giga "http://downloads.sourceforge.net/sourceforge/libpng/libpng-${LIBPNG_VERSION}.tar.gz" \
 && echo "e2b5e1b4329650992c041996cf1269681b341191dc07ffed816c555769cceb77  libpng-${LIBPNG_VERSION}.tar.gz" | sha256sum -c

# Build static Win32 libpng
RUN tar xf "libpng-${LIBPNG_VERSION}.tar.gz" \
 && cd "libpng-${LIBPNG_VERSION}" \
 && ./configure --prefix=/usr/x86_64-w64-mingw32 --host=x86_64-w64-mingw32 --disable-shared \
 && make -j"$(nproc)" \
 && make install

# Build Win32 spice_vdagent
COPY ./ ./spice_vdagent_win32
RUN cd spice_vdagent_win32 \
 && autoreconf -fiv \
 && env LIBPNG_CFLAGS="-I/usr/x86_64-w64-mingw32/include/libpng16" LIBPNG_LIBS="-lpng16 -lz" ZLIB_CFLAGS=" " ZLIB_LIBS="-lz" ./configure --prefix=/usr/x86_64-w64-mingw32 --host=x86_64-w64-mingw32 \
 && make -j"$(nproc)" \
 && make install
